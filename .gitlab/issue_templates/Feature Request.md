<!-- markdownlint-disable MD041 -->
## Summary

(Summarize your feature request concisely.)

## How would you like the feature to work ?

(You should [Create a Test Qase](https://app.qase.io/case/STC/create/33) and provide the corresponding link.)

## Impact of non-completion

(Describe here issues related to the non completion of the requested feature.)

## Useful resources

(Add links to examples, guides, tools, specifications or documents related to your request.)

/label ~"type::feature"
/label ~"project::telepresence" ~"team::ptt"
/label ~"flow::triage"

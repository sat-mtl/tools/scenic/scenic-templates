<!-- markdownlint-disable MD041 -->
## Summary

(Summarize the goal of your analysis, all this file is written in Markdown)

(Please submit this issue without the comments in parenthesis and remove the section that doesn't concern your role)

### What?

- (Provide support for whatever protocol in the Scenic environment)
- (Add an alternative panel in Scenic in order to add new controlers)

### Why?

- (I want to support any kind of initiatives)

### Interests?

- (I developed a new feature in Switcher an I would like to control it with Scenic)
- (I made experimentation with some tools and I have ideas about its support)
- (I want to refactor something here because it will make my life better)

## Useful ressources

(Add all examples, guides, tools, specifications or documentation related to your request)

/label ~"type::analysis"
/label ~"project::telepresence" ~"team::ptt"
/label ~"flow::triage"

<!-- markdownlint-disable MD041 -->
## Summary

(Summarize the bug encountered concisely)

(Also, you should add precisions on the event implied in this bug)

## Versions used to reproduce issue

(Indicate all relevant information about your system, including as many details as possible.)

(**Most importantly, specify which version of the software you are using.**)

- [ ] ScenicOS version: _scenicos-focal-amd64-x86-64-1.0.0-build133-be323984-2022-08-22-1734_ (cat /image_version)

(If versions aren't provided, we take account that everything should be reproduced on the latest _candidate_ tag)

## Steps to reproduce

(You should [Create a Test Qase](https://app.qase.io/case/STC/create/33) and provide the corresponding link.)

## What is the current bug behavior?

(Describe what actually happens)

## What is the expected correct behavior?

(Describe what you should see instead)

## What is the frequency of occurrence of this behavior ?

(Describe if the issue appears every time, sometimes, etc. You can also mention here if this issue is a regression)

- 100%
- 50%
- Flaky
- Depends on a specific configuration
- Regression (can you link the issue?)


## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

## Additional comments

(Add references to related issues and provide any extra comments.)

/label ~"flow::triage"
/label ~"type::bug"
/label ~"project::telepresence" ~"team::ptt"
